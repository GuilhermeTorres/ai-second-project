from itertools import product
import controller_interface
from typing import List, Tuple, Any
from random import *
from math import *
import config

primes = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
		  103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193)

ACTIONS = [1, 2, 3, 4, 5]
INDEX = 0
VALUE = 1

ALPHA = .2 #.5, .2 , .9
GAMMA = .9 #.9, .1

class State(controller_interface.State):
	def __init__(self, sensors: dict):
		self.sensors = sensors

	def compute_features(self) -> Tuple:
		"""
		This function should take the raw sensor information of the drill (see below) and compute useful features for
		selecting an action.
		The drill has the following sensors:

		:param sensors: contains:
			 'asteroid_position': (x, y)
			 'asteroid_velocity': (n, m)
			 'asteroid_resources': 0 - ???
			 'align_asteroid': 0 or 1
			 'align_mothership': 0 or 1
			 'drill_angle': angle in rad
			 'drill_position': (x, y)
			 'drill_velocity': (n, m)
			 'drill_mothership_position': (x, y)
			 'drill_resources': 0 - ???
			 'drill_touching_asteroid': 0 or 1
			 'drill_touching_mothership': 0 or 1
			 'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
			 'drill_edge_position': (x, y)
			 'drill_gas': 0 - MAX_GAS (defined in config.py)
			 'enemy_1_drill_angle': angle in rad
			 'enemy_1_drill_position': (x, y)
			 'enemy_1_drill_velocity': (n, m)
				 'enemy_1_drill_mothership_position': (x, y)
			 'enemy_1_drill_resources': 0 - 0
			 'enemy_1_drill_touching_asteroid': 0 or 1
			 'enemy_1_drill_touching_mothership': 0 or 1
			 'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
			 'enemy_1_drill_edge_position': (x, y)
			 'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
		:return: A Tuple containing the features you defined
		"""

		# Features

		def tovector(p, q) -> tuple:
			# return a vector between two points

			return q[0] - p[0], q[1] - p[1]

		def norm(v) -> int:
			#return the norm of a vector 2d

			return sqrt(pow(v[0], 2) + pow(v[1], 2))

		def dist(p, q) -> int:
			# return the distance of two points

			return norm(tovector(p, q))

		def getorientation(theta) -> tuple:
			# return a vector of norm 1 with the orientation of the drill

			return cos(theta), sin(theta)

		def cross(a, b) -> int:
			# return the cross product of two vectors

			return a[0]*b[1] - a[1]*b[0]

		def ccw(p, q, r) -> bool:
			# return true if point r is at left of straight pq

			return cross(tovector(p, q), tovector(p, r)) > 0

		def rightorleft(drill_edge_position, drill_angle, target_position) -> bool:
			# return true if drill should turn right or false if should turn left

			drill_orientation = getorientation(drill_angle-(pi/2))
			r = drill_edge_position[0] + drill_orientation[0], drill_edge_position[1] + drill_orientation[1]
			return ccw(drill_edge_position, target_position, r)

		def rla(drill_edge_position, drill_angle, target_position, align) -> bool:
			# return 0.5 if aligned with target else return the direction to turn
			if (align):
				return 0.5
			else:
				return rightorleft(drill_edge_position, drill_angle, target_position)

		def needDischarge(cooldown, enemy_touching_asteroid) -> bool:
			# return true if the discharge should be use or false if no
			return cooldown == 0 and enemy_touching_asteroid

		def needGas(drill_gas) -> bool:
			# return true if the dril have poor gas
			return drill_gas <= 50

		toasteroid = dist(self.sensors['drill_edge_position'], self.sensors['asteroid_position'])

		aligntoasteroid = rla(self.sensors['drill_edge_position'], self.sensors['drill_angle'], self.sensors['asteroid_position'], self.sensors['align_asteroid'])

		touchingasteroid = self.sensors['drill_touching_asteroid']

		tomothership = dist(self.sensors['drill_edge_position'], self.sensors['drill_mothership_position'])

		aligntomothership = rla(self.sensors['drill_edge_position'], self.sensors['drill_angle'], self.sensors['drill_mothership_position'], self.sensors['align_mothership'])

		touchingmothership = self.sensors['drill_touching_mothership']

		gas = needGas(self.sensors['drill_gas'])

		return (toasteroid, aligntoasteroid, touchingasteroid, tomothership, aligntomothership, touchingmothership, gas)

	def discretize_features(self, features: Tuple) -> Tuple:
		"""
		This function should map the (possibly continuous) features (calculated by compute features) and discretize
		them.
		:param features
		:return: A tuple containing the discretized features
		"""

		def maxdist():
			# return the max distance that two objects could be have

			return sqrt(pow(config.WIDTH, 2) + pow(config.HEIGHT, 2))

		def norm_dist(distance):
			# return the normalized distance

			mdist = maxdist()

			if (mdist/2 < distance <= mdist):
				return 0
			elif (mdist/4 < distance <= mdist/2):
				return 1
			elif (mdist/8 < distance <= mdist/4):
				return 2
			elif (mdist/16 < distance <= mdist/8):
				return 3
			else:
				return 4

		def discretize_ternary(param, min, max):
			# return the ternary value of a parameter

			if (param <= min):
				return 0
			elif (min < param < max):
				return 1
			else:
				return 2

		toasteroid = norm_dist(features[0])

		aligntoasteroid = discretize_ternary(features[1], 0.0, 1.0)

		touchingasteroid = features[2]

		tomothership = norm_dist(features[3])

		aligntomothership = discretize_ternary(features[4], 0.0, 1.0)

		touchingmothership = features[5]

		gas = features[6]

		return (toasteroid, aligntoasteroid, touchingasteroid, tomothership, aligntomothership, touchingmothership, gas)

	@staticmethod
	def discretization_levels() -> Tuple:
		"""
		This function should return a vector specifying how many discretization levels to use for each state feature.
		:return: A tuple containing the discretization levels of each feature
		"""
		return (5, 3, 2, 5, 3, 2, 2)

	def get_current_state(self):
		"""
		:return: computes the discretized features associated with this state object.
		"""
		features = self.discretize_features(self.compute_features())
		return features

	@staticmethod
	def get_state_id(discretized_features: Tuple) -> int:
		"""
		Handy function that calculates an unique integer identifier associated with the discretized state passed as
		parameter.
		:param discretized_features
		:return: unique key
		"""

		terms = [primes[i] ** discretized_features[i] for i in range(len(discretized_features))]

		s_id = 1
		for i in range(len(discretized_features)):
			s_id = s_id * terms[i]

		return s_id

	@staticmethod
	def get_number_of_states() -> int:
		"""
		Handy function that computes the total number of possible states that exist in the system, according to the
		discretization levels specified by the user.
		:return:
		"""
		v = State.discretization_levels()
		num = 1

		for i in (v):
			num *= i

		return num

	@staticmethod
	def enumerate_all_possible_states() -> List:
		"""
		Handy function that generates a list with all possible states of the system.
		:return: List with all possible states
		"""

		levels = State.discretization_levels()

		levels_possibilities = [(j for j in range(i)) for i in levels]

		return [i for i in product(*levels_possibilities)]


class QTable(controller_interface.QTable):

	table = dict()

	def __init__(self):
		"""
		This class is used to create/load/store your Q-table. To store values we strongly recommend the use of a Python
		dictionary.
		"""

		toasteroid = [0, 1, 2, 3, 4]

		aligntoasteroid = [0, 1, 2]

		touchingasteroid = [0, 1]

		tomothership = [0, 1, 2, 3, 4]

		aligntomothership = [0, 1, 2]

		touchingmothership = [0, 1]

		gas = [0,1]

		for key in list(product(toasteroid, aligntoasteroid, touchingasteroid, tomothership, aligntomothership, touchingmothership, gas)):
			self.table[key] = [0.0, 0.0, 0.0, 0.0, 0.0]


	def get_q_value(self, key: State, action: int) -> float:
		"""
		Used to securely access the values within this q-table
		:param key: a State object
		:param action: an action
		:return: The Q-value associated with the given state/action pair
		"""
		el = self.table[key.get_current_state()]

		return el[action-1]

	def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
		"""
		Used to securely set the values within this q-table
		:param key: a State object
		:param action: an action
		:param new_q_value: the new Q-value to associate with the specified state/action pair
		:return:
		"""
		el = self.table[key.get_current_state()]

		el[action-1] = new_q_value

		self.table[key.get_current_state()] = el

	def max_q_value(self, key: State) -> tuple:
		"""
		:param key: a State object
		:return: the max value in the key state
		"""
		el = self.table[key.get_current_state()]
		max_value = max(el)
		index = el.index(max_value)
		return index, max_value


	@staticmethod
	def load(path: str) -> "QTable":
		"""
		This method should load a Q-table from the specified file and return a corresponding QTable object
		:param path: path to file
		:return: a QTable object
		"""
		try:
			f = open(path, "r")
			text = f.read()
			lines = text.split('\n')
			lines = lines[1:]
			new_table = dict()
			for line in lines:
			  l = line.split(", ")
			  key = tuple([int(e) for e in l[0:7]])
			  new_table[key] = [float(e) for e in l[7:]]
			f.close()
			QT = QTable()
			QT.table = new_table
			return QT
		except:
			print("Error in read file")
			return QTable()

	def save(self, path: str, *args) -> None:
		"""
		This method must save this QTable to disk in the file file specified by 'path'
		:param path:
		:param args: Any optional args you may find relevant; beware that they are optional and the function must work
					 properly without them.
		"""
		try:
			f = open(path,"w")
			f.write("toasteroid, aligntoasteroid, touchingasteroid, tomothership, aligntomothership, touchingmothership, gas, right, left, accelerate, discharge, nothing")
			keys = self.table.keys()
			for key in keys:
				f.write('\n')
				el = self.table[key]
				f.write(', '.join(str(e) for e in key))
				f.write(', ')
				f.write(', '.join(str(e) for e in el))
			f.close()
		except:
			print("Error in write file")


class Controller(controller_interface.Controller):

	def __init__(self, q_table_path: str):
		if q_table_path is '':
			self.q_table = QTable()
		else:
			self.q_table = QTable.load(q_table_path)

	def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_episode: bool) -> None:
		"""
		This method is called by the learn() method in simulator.Simulation() to update your Q-table after each action
		is taken.
		:param new_state: The state the drill just entered
		:param old_state: The state the drill just left
		:param action: the action the drill performed to get to new_state
		:param reward: the reward the drill received for getting to new_state
		:param end_of_episode: boolean indicating if an episode timeout was reached
		"""
		new_value = ((1.0-ALPHA) * self.q_table.get_q_value(old_state, action)) + (ALPHA * (reward + GAMMA * (self.q_table.max_q_value(new_state)[VALUE])))
		self.q_table.set_q_value(old_state, action, new_value)
		#if(end_of_episode):
		#	self.cont+=1			
		#	self.q_table.save("Statistics/QTable_" + str(self.cont) + ".csv")

	def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
					   end_of_episode: bool) -> float:
		"""
		This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to
		the agent.
		:param new_state: The state the drill just entered
		:param old_state: The state the drill just left
		:param action: the action the drill performed to get in new_state
		:param n_steps: number of steps the drill has taken so far in the current episode
		:param end_of_episode: boolean indicating if an episode timeout was reached
		:return: The reward to be given to the agent
		"""

		def firstmodel(old_state: list, new_state: list, action: int):
			reward = 0.0
			gasnew = new_state[6]
			if(gasnew == False):
				toasteroidold = old_state[0]
				toasteroidnew = new_state[0]
				if(toasteroidold > toasteroidnew):
					reward += .5
				elif(toasteroidold < toasteroidnew):
					reward -= .5
				else:
					reward -= .2
				aligntoasteroidnew = new_state[1]
				aligntoasteroidold = old_state[1]
				if(aligntoasteroidnew == 1 and aligntoasteroidold != 1):
					reward += .5
				right = 1
				left = 2
				if(aligntoasteroidold == 0 and action == right):
					reward -= .5
				elif(aligntoasteroidold == 0 and action == left):
					reward += .5
				if(aligntoasteroidold == 2 and action == left):
					reward -= .5
				elif(aligntoasteroidold == 2 and action == right):
					reward += .5
				touchingasteroidold = old_state[2]
				accelerate = 3
				if(aligntoasteroidold == 1 and action == accelerate and touchingasteroidold == False):
					reward += .5
				touchingasteroidnew = new_state[2]
				if(touchingasteroidnew == True):
					reward += 1.0
			else:
				tomothershipold = old_state[3]
				tomothershipnew = new_state[3]
				if(tomothershipold > tomothershipnew):
					reward += .5
				elif(tomothershipold < tomothershipnew):
					reward -= .5
				else:
					reward -= .2
				aligntomothershipnew = new_state[4]
				aligntomothershipold = old_state[4]
				if(aligntomothershipnew == 1 and aligntomothershipold != 1):
					reward += .5
				right = 1
				left = 2
				if(aligntomothershipold == 0 and action == right):
					reward -= .5
				elif(aligntomothershipold == 0 and action == left):
					reward += .5
				if(aligntomothershipold == 2 and action == left):
					reward -= .5
				elif(aligntomothershipold == 2 and action == right):
					reward += .5
				touchingmothershipold = old_state[5]
				accelerate = 3
				if(aligntomothershipold == 1 and action == accelerate and touchingmothershipold == False):
					reward += .5				
				touchingmothershipnew = new_state[5]
				if(touchingmothershipnew == True):
					reward += 1.0
			gasold = old_state[6]
			if (gasold == True and gasnew == False):
				reward += 1.0

			return reward

		def secondmodel(new_state: list, action: int):
			# Features : (toasteroid, aligntoasteroid, touchingasteroid, tomothership, aligntomothership, touchingmothership, gas)

			reward = 0.0

			if (new_state[2] == 1 and action == 5 and new_state[6] == 0): # Minerando e não precisa de gas
				reward += 1.0

			elif (new_state[1] == 1 and action == 3 and new_state[6] == 0): # Acelera se alinhado asteróide e não precisa de gas
				reward += 1.0
			elif (new_state[4] == 1 and action == 3 and new_state[6] == 1): # Acelera se alinhado mothership e precisa de gas
				reward += 1.0

			elif (new_state[1] == 2 and action == 1 and new_state[6] == 0): # Vira para direita se não precisa de gas e não está alinhado o asteróide
				reward += 1.0
			elif (new_state[1] == 0 and action == 2 and new_state[6] == 0): # Vira para esquerda se não precisa de gas e não está alinhado o asteróide
				reward += 1.0

			elif (new_state[4] == 2 and action == 1 and new_state[6] == 1): # Vira para direita se precisa de gas e não está alinhado a mothership
				reward += 1.0
			elif (new_state[4] == 0 and action == 2 and new_state[6] == 1): # Vira para esquerda se precisa de gas e não está alinhado a mothership
				reward += 1.0

			else:
				reward -= 2.0

			return reward

		oldst = old_state.get_current_state()
		newst = new_state.get_current_state()

		# return firstmodel(oldst, newst, action)
		return secondmodel(newst, action)

	def take_action(self, new_state: State, episode_number: int) -> int:
		"""
		Decides which action the drill must execute based on its Q-Table and on its exploration policy
		:param new_state: The current state of the drill
		:param episode_number: current episode during the training period
		:return: The action the drill chooses to execute

        1 - Right
        2 - Left
        3 - Accelerate forward
        4 - Discharge
        5 - Nothing
		"""

		def maxaction(q_table: QTable, new_state: State) -> int:
			maxaction = 1
			maxqvalue = q_table.get_q_value(new_state, maxaction)
			for action in ACTIONS:
				if maxqvalue < q_table.get_q_value(new_state, action):
					maxaction = action
					maxqvalue = q_table.get_q_value(new_state, action)
			return maxaction

		def boltzmanexploration(q_table: QTable, new_state: State) -> int:
			l = []
			temperature = 10 #10, 20
			for action in ACTIONS:
				l.append(exp(q_table.get_q_value(new_state, action))/temperature)
			p = [e/sum(l) for e in l]
			f = []
			f.append(p[0])
			for i in range(1, len(p)):
				f.append(p[i] + f[i-1])
			r = random()
			for i in range(0, len(f)):
				if r <= f[i]:
					return ACTIONS[i]
			return 5

		return maxaction(self.q_table, new_state)
		# return boltzmanexploration(self.q_table, new_state)
